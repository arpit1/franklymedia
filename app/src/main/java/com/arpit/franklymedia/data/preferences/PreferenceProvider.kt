package com.arpit.franklymedia.data.preferences

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import kotlin.collections.ArrayList

class PreferenceProvider(var context: Context) {
    private var PRIVATE_MODE = 0
    private val preference: SharedPreferences
        get() =
            context.getSharedPreferences("frankly_media", PRIVATE_MODE)

//    fun saveData(key: String, value: String) {
//        preference.edit().putString(key, value).apply()
//    }
//
//    fun getData(key: String): String? {
//        return preference.getString(key, null)
//    }

    fun saveImagesJsonObject(imagesObj: ArrayList<String?>) {
        val prefsEditor = preference.edit()
        val gson = Gson()
        val json = gson.toJson(imagesObj)
        prefsEditor.putString("MyImagesObject", json)
        prefsEditor.apply()
    }

    fun getImagesJsonObject(): ArrayList<String?>? {
        val gson = Gson()
        val json = preference.getString("MyImagesObject", "")
        val type: Type =
            object : TypeToken<List<String?>?>() {}.type
        return gson.fromJson(json, type)
    }
}