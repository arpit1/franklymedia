package com.arpit.franklymedia.data.response

data class Images(
    var message: String? = null,
    var status: String? = null
)