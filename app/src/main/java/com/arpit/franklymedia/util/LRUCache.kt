package com.arpit.franklymedia.util

import com.arpit.franklymedia.data.preferences.PreferenceProvider
import kotlin.collections.ArrayList

// store references of key in cache
var list: ArrayList<String?> = ArrayList()

// maximum capacity of cache
var heapSize = 20

fun initializeList(preferenceProvider: PreferenceProvider) {
    preferenceProvider.getImagesJsonObject()?.let {
        list = it
    }
}

/* Refers key x with in the LRU cache */
fun refer(x: String?, preferenceProvider: PreferenceProvider) {
    if (!list.contains(x)) {
        if (list.size == heapSize) {
            list.removeAt(0)
        }
    } else {
        /* The found page may not be always the last element, even if it's an
           intermediate element that needs to be removed and added to the start
           of the Queue */
        if (list.contains(x)) {
            list.remove(x)
        }
    }
    list.add(x)
    preferenceProvider.saveImagesJsonObject(list)
}

// display contents of cache
fun display(): ArrayList<String?> {
    list.forEach{
        println("xhxhxhxh $it ")
    }
    return list
}

// clear contents of cache
fun clear(preferenceProvider: PreferenceProvider) {
    list.clear()
    list = ArrayList()
    preferenceProvider.saveImagesJsonObject(list)
}