package com.arpit.franklymedia.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel: ViewModel() {

    var buttonClicked: MutableLiveData<String> = MutableLiveData()

    fun onGenerateDogButtonClick() {
        buttonClicked.postValue("GenerateDog")
    }

    fun onGenerateDogsButtonClick() {
        buttonClicked.postValue("GeneratedDogs")
    }
}