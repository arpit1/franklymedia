package com.arpit.franklymedia.ui.generate

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arpit.franklymedia.data.preferences.PreferenceProvider
import com.arpit.franklymedia.network.ioScope
import com.arpit.franklymedia.repository.GetImageRepository
import com.arpit.franklymedia.util.ApiException
import com.arpit.franklymedia.util.NoInternetException
import kotlinx.coroutines.launch

class GenerateImageViewModel(
    private val imageRepository: GetImageRepository
): ViewModel() {

    val onSuccess = MutableLiveData<Any>()
    val onFailure = MutableLiveData<String>()

    fun onGenerateButtonClick() {
        ioScope.launch {
            try {
                val response = imageRepository.getImages()
                response.let {
                    val result = it
                    onSuccess.postValue(result)
                    return@launch
                }
            } catch (ex: ApiException) {
                ex.printStackTrace()
                onFailure.postValue(ex.message.toString())
            } catch (ex: NoInternetException) {
                ex.printStackTrace()
                onFailure.postValue(ex.message.toString())
            }
        }
    }
}