package com.arpit.franklymedia.ui.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.arpit.franklymedia.R
import com.arpit.franklymedia.data.preferences.PreferenceProvider
import com.arpit.franklymedia.databinding.ActivityMainBinding
import com.arpit.franklymedia.ui.cached.ViewCachedImagesActivity
import com.arpit.franklymedia.ui.generate.GenerateImageActivity
import com.arpit.franklymedia.util.initializeList
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class HomeActivity : AppCompatActivity() , KodeinAware {

    override val kodein by kodein()

    private lateinit var viewModel: HomeViewModel
    private val preferenceProvider: PreferenceProvider by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBinding()
    }

    private fun setBinding() {
        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        binding.viewmodel = viewModel

        observeValue()

        initializeList(preferenceProvider)
    }

    private fun observeValue() {
        viewModel.buttonClicked.observe(this, Observer { it ->
            if (!it.isNullOrEmpty()) {
                when (it) {
                    "GenerateDog" -> {
                        Intent(this, GenerateImageActivity::class.java).also {
                            startActivity(it)
                            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                        }
                    }
                    "GeneratedDogs" -> {
                        Intent(this, ViewCachedImagesActivity::class.java).also {
                            startActivity(it)
                            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                        }
                    }
                }
            }
        })
    }
}
