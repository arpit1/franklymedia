package com.arpit.franklymedia.ui.generate

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.arpit.franklymedia.R
import com.arpit.franklymedia.data.preferences.PreferenceProvider
import com.arpit.franklymedia.data.response.Images
import com.arpit.franklymedia.databinding.ActivityGenerateImageBinding
import com.arpit.franklymedia.util.refer
import com.arpit.franklymedia.util.showToast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_generate_image.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class GenerateImageActivity : AppCompatActivity() , KodeinAware {

    override val kodein by kodein()

    private lateinit var viewModel: GenerateImageViewModel

    private val factory: GenerateImageViewModelFactory by instance()
    private val preferenceProvider: PreferenceProvider by instance()

    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBinding()
    }

    private fun setBinding() {
        val binding: ActivityGenerateImageBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_generate_image)

        viewModel = ViewModelProvider(this, factory).get(GenerateImageViewModel::class.java)
        binding.viewmodel = viewModel

        observeValue()
        setToolbar()
    }

    private fun observeValue() {
        viewModel.onSuccess.observe(this, Observer {
            val imageObj = it as Images
            if(imageObj.status?.toLowerCase(Locale.ENGLISH)?.equals("success")!!) {
                val mPicasso = Picasso.get()
                mPicasso.setIndicatorsEnabled(false)
                refer(imageObj.message, preferenceProvider)
                mPicasso.load(imageObj.message).fit().centerInside().error(R.mipmap.fm_icon)
                    .placeholder(R.mipmap.fm_icon)
                    .into(imageView)
            } else {
                showToast("Something went wrong")
            }
        })
        viewModel.onFailure.observe(this, Observer { it ->
            showToast(it)
        })
    }

    private fun setToolbar() {
        toolbar = findViewById(R.id.toolbar)
        toolbar.title = resources.getString(R.string.generateDog)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.black))
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        toolbar.setNavigationIcon(R.drawable.back_arrow_black)
        setSupportActionBar(toolbar)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item!!)
    }
}