package com.arpit.franklymedia.ui.cached

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arpit.franklymedia.data.preferences.PreferenceProvider
import com.arpit.franklymedia.util.clear

class ViewCachedImageViewModel(
    private val preferenceProvider: PreferenceProvider
): ViewModel() {

    val clearImages = MutableLiveData<String>()

    fun onClearButtonClick() {
        clear(preferenceProvider)
        clearImages.value = "Clear"
    }
}