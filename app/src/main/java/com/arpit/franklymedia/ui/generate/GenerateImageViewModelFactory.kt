package com.arpit.franklymedia.ui.generate

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.arpit.franklymedia.data.preferences.PreferenceProvider
import com.arpit.franklymedia.repository.GetImageRepository

@Suppress("UNCHECKED_CAST")
class GenerateImageViewModelFactory(
    private val getImageRepository: GetImageRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return GenerateImageViewModel(getImageRepository) as T
    }
}