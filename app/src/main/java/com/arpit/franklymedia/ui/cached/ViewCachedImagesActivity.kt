package com.arpit.franklymedia.ui.cached

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.arpit.franklymedia.R
import com.arpit.franklymedia.data.preferences.PreferenceProvider
import com.arpit.franklymedia.databinding.ActivityViewCachedImagesBinding
import com.arpit.franklymedia.util.display
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_view_cached_images.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*
import kotlin.collections.ArrayList


class ViewCachedImagesActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()

    private lateinit var viewModel: ViewCachedImageViewModel

    private val factory: ViewCachedImageViewModelFactory by instance()
    private val preferenceProvider: PreferenceProvider by instance()

    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBinding()
    }

    private fun setBinding() {
        val binding: ActivityViewCachedImagesBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_view_cached_images)

        viewModel = ViewModelProvider(this, factory).get(ViewCachedImageViewModel::class.java)
        binding.viewmodel = viewModel

        observeValue()
        setToolbar()

        showImages()
    }

    private fun observeValue() {
        viewModel.clearImages.observe(this, Observer { it ->
            linearLayout.removeAllViews()
        })
    }

    private fun showImages() {
        val list = display()
        val mPicasso = Picasso.get()
        mPicasso.setIndicatorsEnabled(false)

        for(i in list.size-1 downTo 0) {
            val imageViewLayout: View = layoutInflater.inflate(R.layout.image_layout, null)
            val imageView: ImageView = imageViewLayout.findViewById(R.id.image)
            mPicasso.load(list[i]).error(R.mipmap.fm_icon)
                .fit()
                .placeholder(R.mipmap.fm_icon)
                .into(imageView)

            linearLayout.addView(imageViewLayout)
        }
    }

    private fun setToolbar() {
        toolbar = findViewById(R.id.toolbar)
        toolbar.title = resources.getString(R.string.generated)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.black))
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        toolbar.setNavigationIcon(R.drawable.back_arrow_black)
        setSupportActionBar(toolbar)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item!!)
    }
}