package com.arpit.franklymedia.ui.cached

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.arpit.franklymedia.data.preferences.PreferenceProvider
import com.arpit.franklymedia.repository.GetImageRepository

@Suppress("UNCHECKED_CAST")
class ViewCachedImageViewModelFactory(
    private val preferenceProvider: PreferenceProvider
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ViewCachedImageViewModel(preferenceProvider) as T
    }
}