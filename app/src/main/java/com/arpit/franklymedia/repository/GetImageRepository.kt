package com.arpit.franklymedia.repository

import com.arpit.franklymedia.data.response.Images
import com.arpit.franklymedia.network.MyApi
import com.arpit.franklymedia.network.SafeApiRequest

class GetImageRepository(
    private val api: MyApi
): SafeApiRequest() {

    suspend fun getImages(): Images {
        return apiRequest { api.getImage() }
    }
}