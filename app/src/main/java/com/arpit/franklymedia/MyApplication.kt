package com.arpit.franklymedia

import android.app.Application
import com.arpit.franklymedia.data.preferences.PreferenceProvider
import com.arpit.franklymedia.network.MyApi
import com.arpit.franklymedia.network.NetworkConnectionInterceptor
import com.arpit.franklymedia.repository.GetImageRepository
import com.arpit.franklymedia.ui.cached.ViewCachedImageViewModelFactory
import com.arpit.franklymedia.ui.generate.GenerateImageViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class MyApplication: Application(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@MyApplication))

        bind() from singleton { MyApi(instance()) }

        bind() from singleton { NetworkConnectionInterceptor(instance()) }

        bind() from singleton { PreferenceProvider(instance()) }

        bind() from singleton { GetImageRepository(instance()) }

        bind() from provider { GenerateImageViewModelFactory(instance()) }

        bind() from provider { ViewCachedImageViewModelFactory(instance()) }
    }
}