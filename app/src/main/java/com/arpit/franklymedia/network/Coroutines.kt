package com.arpit.franklymedia.network

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

val ioScope: CoroutineScope = CoroutineScope(Dispatchers.IO)
val mainScope: CoroutineScope = CoroutineScope(Dispatchers.Main)