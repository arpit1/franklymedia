package com.arpit.franklymedia.customviews

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import androidx.appcompat.widget.AppCompatTextView
import com.arpit.franklymedia.R

class CustomTextViewBold(context: Context, attrs: AttributeSet): AppCompatTextView(context, attrs) {
    init {
        val face = Typeface.createFromAsset(context.assets, "fonts/SourceSansPro_Semibold.otf")
        this.typeface = face
        this.setTextSize(
            TypedValue.COMPLEX_UNIT_PX,
            resources.getDimension(R.dimen.text_size_medium)
        )
    }
}